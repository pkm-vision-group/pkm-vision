__author__ = "Piotr Winkler"
__version__ = 5.1

import argparse
from detection_functions import detect_common_objects, draw_bbox, bfmatch
from feature_map import *

cnt = 0
cnt2 = 0
station = None
startpoint = 0
database = read_data('database2.txt')


def detect(frame, points, empty_frames, distance):
    """
    This function passes frames of record received from GUI forward to detection_functions for processing
    in neural network. It also manages the work of "BF match" algorithm which is used to detect which station was
    reached by the train after the neural network indicated that train reached some station.
    :param frame:
        Current frame of record.
    :param points:

    :param empty_frames:
    :return:
        Output image with bounding boxes on it. List of labels for detected objects. Station number which
        was recognized by "BF match" algorithm.
    """
    args = parse_args()
    global cnt
    global cnt2
    global station
    global startpoint
    global database

    # apply object detection
    bbox, label, conf, centers = detect_common_objects(frame, args.model, args.weights, args.names,
                                              args.conf, args.nms, args.scale)

    # draw bounding box over detected objects
    out = draw_bbox(frame, bbox, label, conf, args.names)
    feature_distance = 0
    # guess distance from feature map and update startpoint
    if distance > 30:
        database = read_data('database2.txt')
        startpoint = distance - 30
        endpoint = distance + 50
        feature_distance = compare_data(database, bbox, label, startpoint, endpoint)

    # check station
    if 'station' in label and cnt < 10:
        cnt += 1
        station = bfmatch(frame, cnt)
    elif 'station' not in label and cnt2 < 30 and cnt > 1:
        cnt2 += 1
    elif 'station' not in label:
        cnt = 0
        cnt2 = 0
        station = None

    return out, label, station, points, empty_frames, feature_distance


def parse_args():
    """
    This function allows to change program parameters while running from command line.
    :return:
        Object containing values of all listed arguments.
    :Example:
        To run program with e.g. confidence threshold value equal to 0.5 and scale value equal to 0.1
        go to project directory and type in command line:
            python detection.py -c 0.5 -s 0.1
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('--video', '-v', help='path to input video',
                        default='PKM1video.avi')
    parser.add_argument('--model', '-m', help='path to neural network model',
                        default='tiny-yolo-voc.cfg')
    parser.add_argument('--weights', '-w', help='path to neural network weights',
                        default='tiny-yolo-voc_500000.weights')
    parser.add_argument('--names', '-n', help='path to file containing names of classes',
                        default='obj.names.txt')
    parser.add_argument('--conf', '-c', help='confidence threshold value', type=float,
                        default=0.45)
    parser.add_argument('--nms', '-t', help='non-maximum suppression threshold value', type=float,
                        default=0.4)
    parser.add_argument('--scale', '-s', help='scale of input frames', type=float,
                        default=0.01)

    return parser.parse_args()
