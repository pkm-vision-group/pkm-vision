
czulosc_skretu = 30
nr_klatki = 0
probka = 0
dlugosc_probkowania = 5
na_zakrecie = False
poczatek = 0
koniec = 0

def tracking(bbox):
    """
    Algorytm śledzenia torów na nagraniu, z możliwością zmiany ilości klatek, z których wyciągane będzie średnie położenie
    oraz czułość mówiąca jaka zmiana położenia bbox jest uznawana za zakręt, co pozwala na eliminację szumów.

    :param bbox: ROI
    """
    punkt_odniesienia = bbox[0]
    global czulosc_skretu
    global nr_klatki

    global probka
    global dlugosc_probkowania
    global na_zakrecie
    global poczatek
    global koniec

    message = ""

    nr_klatki += 1

    if nr_klatki <= dlugosc_probkowania:
        probka += int(bbox[0])
        if nr_klatki == dlugosc_probkowania:
            srednie_obecne_polozenie = probka / dlugosc_probkowania
            zmiana_polozenia = srednie_obecne_polozenie - punkt_odniesienia
            #print(zmiana_polozenia)
            if zmiana_polozenia > 0:
                kierunek = "prawo"
            else:
                kierunek = "lewo"
            if abs(zmiana_polozenia) > czulosc_skretu and na_zakrecie is False:
                na_zakrecie = True
                poczatek += 1
                #print("poczatek zakretu w " + kierunek + "%i"%poczatek)
                message = "Poczatek zakretu"
            elif abs(zmiana_polozenia) < czulosc_skretu and na_zakrecie is True:
                na_zakrecie = False
                koniec += 1
                #print("koniec zakretu %i" % koniec)
                message = "Koniec zakretu"
            if poczatek == 2 and koniec == 2:
                czulosc_skretu = 5
            elif poczatek == 5 and koniec == 5:
                czulosc_skretu = 10

            probka = 0
            nr_klatki = 0

        # cv2.putText(frame, tracker_type + " Tracker", (100, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50, 170, 50), 2)
        # cv2.putText(frame, "FPS : " + str(int(fps)), (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50, 170, 50), 2)

    return message
