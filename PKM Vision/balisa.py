import time


class Balis_interpreter:

    def __init__(self, parent):
        self.last = '0'
        self.parent = parent

    def data_input(self, data):
        """
                Function that performs operations basing on signals from Hall sensors.

                :param data:
                ID number of a recognised Hall sensor
            """


        self.velocity(data)
        if self.last == "XD":
            return
        elif data == '03020066' or data == '03020065':
            self.parent.balisa_label.setText("Ostatnia balisa: Kielpinek pkm")
            self.parent.distance = 25
            self.parent.checkPointDistance = 0
            self.reset()
        elif data == '03020068' or data == '03020067':
            self.parent.balisa_label.setText("Ostatnia balisa: Mostek")
            self.parent.distance = 205  # 265
            self.reset()
            self.parent.multiplier = 4
        elif data == '03010000':
            self.parent.balisa_label.setText("Ostatnia balisa: Strzyża od mostu")
            self.parent.distance = 748
            self.reset()
            self.parent.multiplier = 2.8
        elif data == '03010067' or data == '03010068':
            self.parent.balisa_label.setText("Ostatnia balisa: Strzyża od Wrzeszcza")
            self.parent.distance = 890
            self.reset()
        elif data == '03010066':
            self.parent.balisa_label.setText("Ostatnia balisa: Skret na Osowa")
            self.parent.distance = 964
            self.reset()
        elif data == '03010066':
            self.parent.balisa_label.setText("Ostatnia balisa: Skręt na wiadukt")
            #self.parent.distance =
            self.reset()
        elif data == '0305006D' or data == '0305006E':
            #print("Wrzeszcz, dwa kolo siebie przed wiaduktem")
            # self.parent.distance =
            self.reset()
        elif data == '0304012E' or data == '0304012F' or data == '030400FF' or data == '03040065':
            #print("Droga na osowe 4 kolo siebie")
            # self.parent.distance =
            self.reset()

    def reset(self):
        """
                Function that resets odometry in parent program.
            """
        self.parent.R = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
        self.parent.t = [[0], [0], [0]]
        self.parent.x_coordinates = [0]
        self.parent.y_coordinates = [0]
        self.parent.stepper = 1
        self.parent.period = 1
        self.parent.total_dist = 0

    def velocity(self, data):
        """
               Function that calculates velocity based on a difference in time
               between two signals from a single Hall sensor.

               :param data:
                   ID number of a recognised Hall sensor
           """
        if self.last != data:
            self.start = time.time()
            self.last = data
        if self.last == data:
            if time.time()-self.start < 0.2:
                pass
            else:
                self.end = time.time()
                self.last = 'XD'
                time_elapsed = self.end-self.start
                velocity = 19.5/time_elapsed
                self.parent.balisa_speed_label.setText("Niedawna prędkość:"+ str(round(velocity, 2)) + " cm/s")
