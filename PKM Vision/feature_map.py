import re
import numpy as np


def read_data(path):
    """
    Reads data from file located in given path and converts it into list of bounding boxes, labels and distance.
    Collected data are preprocessed to use in feature maps.
    :param path: path to database of bounding boxes, labels and distance
    :return: preprocessed list of bounding boxes[0], labels[1], distance[2]
    """
    database = []
    with open(path, 'r+') as file:
        content = file.readlines()
    for line in content:
        data, distance = line.split('\t')
        bbox = re.findall('\[\[(.*?)\]\]', data)
        labels = re.findall('\[(.*?)\]', data.split(']] ')[1])
        bboxs = bbox[0].split('],')
        bboxs = [list(np.fromstring(bboxs[i].replace('[', ''), dtype=int, sep=',')) for i in range(len(bboxs))]

        distance = float(distance)
        labels = encode_labels(labels)
        database.append([bboxs, labels, distance])

    return database


def compare_data(database, bboxes, label, startpoint, endpoint):
    offset = 120
    offset_y = 150
    confidence = 0.7
    close_point = startpoint
    for n, record in enumerate(database):
        if record[2] > startpoint:
            startpoint = n
            break
    for n, record in enumerate(database):
        if record[2] > endpoint:
            endpoint = n
            break
    if endpoint < 175:
        for n, record in enumerate(database[startpoint:endpoint]):
            n_good = 0
            for i, bbox in enumerate(bboxes):
                x_c = bbox[0] + bbox[2]/2
                y_c = bbox[1] + bbox[3]/2
                # scale = (640+480)/(bbox[2] + bbox[3])
                # offset *= scale
                for j, rec_box in enumerate(record[0]):
                    x_c_base = rec_box[0] + rec_box[2]/2
                    y_c_base = rec_box[1] + rec_box[3]/2
                    if x_c_base - offset < x_c < x_c_base + offset and y_c_base - offset_y < y_c < y_c_base + offset_y:
                        if label[i] == record[1][j]:
                            n_good += 1
            # if n_good > 0: print(n_good)
            if n_good != 0:
                pass
            if n_good > confidence*len(bboxes):

                print(f"Distance from starting point {record[2]}")
                close_point += n
                return record[2]
            return 0
    return 0




def encode_labels(labels):
    """
    Encodes labels into valid ones
    :param labels: not valid labels
    :return: valid labels
    """
    labels = labels[0].split(',')
    for i, label in enumerate(labels):
        labels[i] = re.sub("['\s]", '', label)

    return labels
