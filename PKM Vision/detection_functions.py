__author__ = "Piotr Winkler"
__version__ = 4.0

import cv2
import numpy as np


initialize1 = True
initialize2 = True
avg = list(np.zeros(6))
net = None
classes = None
COLORS = np.random.uniform(0, 255, size=(80, 3))


def populate_class_labels(names):
    """
    This function extracts names of classes from input .txt file (each class has to be in separate line).
    :param names:
        Path to input .txt file.
    :return:
        List of classes names.
    """
    with open(names, encoding="utf-8") as file:
        cl = [l.strip() for l in file]

    return cl


def get_output_layers(network):
    """
    This function returns names of neural network layers which have unconnected outputs.
    :param network:
        Used neural network.
    :return:
        List of layers names.
    """
    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in network.getUnconnectedOutLayers()]

    return output_layers


def draw_bbox(img, bbox, labels, confidence, names, colors=None, write_conf=False):
    """
    This function uses neural network outputs to draw bounding boxes on processed images.
    :param img:
        Currently processed image.
    :param bbox:
        List of lists. Each inside list contains four coordinates needed to draw each bounding box.
    :param labels:
        List of labels ordered accordingly to the order of bounding boxes.
    :param confidence:
        List of confidence values for each detection.
    :param names:
        Path to input .txt file containing names of classes.
    :param colors:
        List of bounding boxes layers. None by default.
    :param write_conf:
        Change value to True for writing confidence values next to classes names.
    :return:
        Image with proper bounding boxes.
    """
    global COLORS
    global classes
    if classes is None:
        classes = populate_class_labels(names)
    for i, label in enumerate(labels):
        if colors is None:
            color = COLORS[classes.index(label)]            
        else:
            color = colors[classes.index(label)]
        # writing confidence (optional)
        if write_conf:
            label += ' ' + str(format(confidence[i] * 100, '.2f')) + '%'
        cv2.rectangle(img, (bbox[i][0], bbox[i][1]), (bbox[i][2], bbox[i][3]), color, 2)
        cv2.putText(img, label, (bbox[i][0], bbox[i][1]-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    return img


def detect_common_objects(image, model, weights, names, conf, nms, sc):
    """
    This function processes outputs from neural network and orders them in proper containers.
    :param image:
        Currently processed image.
    :param model:
        Path to neural network model saved in .cfg file.
    :param weights:
        Path to neural network weights saved in .weights file.
    :param names:
        Path to input .txt file containing names of classes.
    :param conf:
        Value of confidence threshold.
    :param nms:
        Value of non-maximum suppression threshold.
    :param sc:
        Value of input image scale using in pre-processing.
    :return:
        Lists of bounding boxes, labels and confidence values.
    """
    height, width = image.shape[:2]
    global classes
    global initialize1
    global net

    if initialize1:
        classes = populate_class_labels(names)
        net = cv2.dnn.readNet(weights, model)
        initialize1 = False

    # scale implementation (sc), resizing
    blob = cv2.dnn.blobFromImage(image, sc, (416, 416), (0, 0, 0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(get_output_layers(net))

    class_ids = []
    confidences = []
    boxes = []
    centers = []

    # confidence threshold
    conf_threshold = conf
    # non-maximum suppression threshold
    nms_threshold = nms

    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                centers.append([center_x, center_y])
                w = int(detection[2] * width)
                h = int(detection[3] * height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])

    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

    bbox = []
    label = []
    conf = []

    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        bbox.append([round(x), round(y), round(x+w), round(y+h)])
        label.append(str(classes[int(class_ids[i])]))
        conf.append(confidences[i])
        
    return bbox, label, conf, centers


def bfmatch(frame, cnt):
    """
    This function recognizes which station is reached by train.
    :param frame:
        Current frame of record.
    :param cnt:
        Counter value. When it's equal to 10 information about reached station is passed backward to GUI_detection.
    :return:
        Number which indicates which station was reached by train.
    """
    global initialize2
    global orb
    global bf
    global kp1, des1, kp2, des2, kp3, des3, kp4, des4, kp5, des5, kp6, des6
    global avg

    if initialize2:
        kielpinek1 = cv2.imread('kielpinek1.jpg', 0)
        kielpinek2 = cv2.imread('kielpinek2.jpg', 0)
        strzyza1 = cv2.imread('strzyza1.jpg', 0)
        strzyza2 = cv2.imread('strzyza2.jpg', 0)
        osowa1 = cv2.imread('osowa1.jpg', 0)
        wrzeszcz1 = cv2.imread('wrzeszcz1.jpg', 0)
        orb = cv2.ORB_create()
        kp1, des1 = orb.detectAndCompute(kielpinek1, None)
        kp2, des2 = orb.detectAndCompute(kielpinek2, None)
        kp3, des3 = orb.detectAndCompute(strzyza1, None)
        kp4, des4 = orb.detectAndCompute(strzyza2, None)
        kp5, des5 = orb.detectAndCompute(osowa1, None)
        kp6, des6 = orb.detectAndCompute(wrzeszcz1, None)

        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        initialize2 = False

    kp9, des9 = orb.detectAndCompute(frame, None)

    # Match descriptors.
    matches1 = bf.match(des1, des9)
    matches2 = bf.match(des2, des9)
    matches3 = bf.match(des3, des9)
    matches4 = bf.match(des4, des9)
    matches5 = bf.match(des5, des9)
    matches6 = bf.match(des6, des9)

    # Sort them in the order of their distance.
    matches1 = sorted(matches1, key=lambda x: x.distance)
    matches2 = sorted(matches2, key=lambda x: x.distance)
    matches3 = sorted(matches3, key=lambda x: x.distance)
    matches4 = sorted(matches4, key=lambda x: x.distance)
    matches5 = sorted(matches5, key=lambda x: x.distance)
    matches6 = sorted(matches6, key=lambda x: x.distance)

    dist1 = sum(int(a.distance) for a in matches1[:50])
    dist2 = sum(int(a.distance) for a in matches2[:50])
    dist3 = sum(int(a.distance) for a in matches3[:50])
    dist4 = sum(int(a.distance) for a in matches4[:50])
    dist5 = sum(int(a.distance) for a in matches5[:50])
    dist6 = sum(int(a.distance) for a in matches6[:50])

    avg[0] += dist1
    avg[1] += dist2
    avg[2] += dist3
    avg[3] += dist4
    avg[4] += dist5
    avg[5] += dist6

    if cnt == 10:
        minimum = avg.index(min(avg))
        avg[0] = 0
        avg[1] = 0
        avg[2] = 0
        avg[3] = 0
        avg[4] = 0
        avg[5] = 0

        return minimum
