import numpy as np
import math


def stand(x, y):
    """
    This function uses linear approximation to check if train is standing still.
    :param x:
    List of last few x coordinates from train trajectory.
    :param y:
    List of last few y coordinates from train trajectory.
    :return:
    Summarised distance of last few points from train trajectory from approximated line.
    """
    xl = []
    yl = []
    for i in range(len(x)):
        xl.append(float(x[i]))
        yl.append(float(y[i]))
    xl = np.array(xl)
    yl = np.array(yl)
    pm = np.polyfit(xl, yl, 1)  # pm = [m, c] -> y = mx + c -> y - mx - c = 0
    summ = 0
    for i in range(len(xl)):
        summ += dist(pm, xl[i], yl[i])
    return summ


def dist(pm, x, y):
    """
    This function computes distance of single point from approximated line.
    :param pm:
    Parameters of the approximated line.
    :param x:
    X coordinate of point.
    :param y:
    Y coordinate of point.
    :return:
    Distance between point and line.
    """
    return abs(-pm[0]*x + y - pm[1])/math.sqrt(pm[0]**2 + 1)
