__author__ = "Filip Adamus", "Piotr Winkler, Jan Mikicki"
__version__ = 4.6

import cv2
import sys
import xml.etree.ElementTree as ET
import numpy as np
import matplotlib.pyplot as plt
import math
import paramiko
from PyQt5 import QtCore, QtWidgets
from PyQt5.uic import loadUi
from PyQt5.QtGui import QPixmap, QImage, QIcon
from PyQt5.QtWidgets import QApplication, QGraphicsScene
from GUI_detection import detect
from odometry import odometry
from server import *
from track import *
from stand import *
import threading

"""
This is the main module of PKM Vision which connects to all the other modules and draws UI.
Variables to tweak on site:

self.tracker_bbox                      : in current file - initial guess for tracker bbox
dlugosc_probkowania, czulosc_skretu    : in track.py - turn detection parameters
different parameters (coords, stepper) : in 'Visual odometry' section in display(), current file
self.multiplier                        : in current file - multiplier for odometry distances
plik                                   : current file - source of the video

"""


def create_server(mainWindow):
    server = Server(mainWindow)


class Form(QtWidgets.QMainWindow):
    """
    Main class of the GUI window.
    """
    iterator = 1
    plik = "http://192.168.137.7:8000/stream.mjpg"
    ip = '192.168.137.7'
    #plik = "KO2.avi"

    def __init__(self):
        """
        Initialization of the GUI basic elements and variables.
        """
        server_thread = threading.Thread(target=create_server, args=[self])
        server_thread.start()
        QtWidgets.QMainWindow.__init__(self)
        loadUi('PKM_GUI.ui', self)
        self.counter = "0"
        self.setWindowIcon(QIcon('logo.png'))

        # GUI timer responsible for processing neural network output in each step.
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.display)
        self.video = ""
        self.scene = QGraphicsScene()
        self.station = None

        self.pause.clicked.connect(self.click_pause)
        self.play.clicked.connect(self.click_play)

        self.stream_button.clicked.connect(self.click_stream)

        self.points = []
        self.empty_frames = 0
        self.odometry_distance = 0
        self.prev_kp = None
        self.prev_des = None
        self.prev_img = None
        self.coords = [[0], [0], [0]]
        self.R = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
        self.t = [[0], [0], [0]]
        self.mtx = []
        self.dist = []
        self.stepper = 1
        self.newcameramtx = None
        self.roi = None
        self.first = True
        self.calibration_file = 'calibrationv3.xml'
        self.x_coordinates = [0]
        self.y_coordinates = [0]
        self.seconds = 0

        self.avgSpeed = 0
        self.checkPointTime = 0
        self.checkPointDistance = 0
        self.total_dist = 0
        self.feature_distance = 0

        self.seconds_timer = QtCore.QTimer(self)
        self.seconds_timer.timeout.connect(self.time)

        self.distance = 0
        self.straight = 0
        self.period = 1
        self.multiplier = 2.8
        self.calibrate()

        self.tracker = cv2.TrackerCSRT_create()

        # initial guess for tracker bbox
        self.tracker_bbox = (168, 450, 250, 70)

    def calibrate(self):
        tree = ET.parse(self.calibration_file)
        root = tree.getroot()
        for i in range(3):
            line = [float(root[0][0 + 3 * i].text), float(root[0][1 + 3 * i].text), float(root[0][2 + 3 * i].text)]
            self.mtx.append(line)

        line = []
        for i in range(5):
            line.append(float(root[1][i].text))
        self.dist.append(line)

        self.mtx = np.array(self.mtx)
        self.dist = np.array(self.dist)

        w = 1500
        h = 1500
        self.resize(w, h)

    def compute_newmatrix(self, frame):
        h, w = frame.shape[:2]
        self.newcameramtx, self.roi = cv2.getOptimalNewCameraMatrix(self.mtx, self.dist, (w, h), 1, (w, h))

    def time(self):
        """
        This method is responsible for displaying time elapsed
        in 'stoper' label since the start of video.
        """
        self.seconds += 1
        self.avgSpeed = self.distance / self.seconds

        self.checkPointDistance = self.distance - self.avgSpeed * self.checkPointTime
        self.checkPointDistance = int(round(self.checkPointDistance))
        self.stoper.setText('Czas od startu: ' + str(self.seconds) + "s")
        self.full_distance.setText('Od poczatku trasy: ' + str(round(self.distance)) + "cm")

    def click_stream(self):
        """
        This method remotely connects to the Raspberry Pi (provided it's in the same network)
        and kicks off video streaming script called rpi.py
        """
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            ssh.connect(self.ip, username='pi', password='raspberry')
        except paramiko.ssh_exception.AuthenticationException:
            print('Failed connection!')

        else:
            stdin, stdout, stderr = ssh.exec_command('python3 rpi.py')

        ssh.close()

    def click_play(self):
        """
        This method contains operations which are connected with "Play" button. This button reads video file and starts
        the timer which is responsible for communicating with neural network.
        """
        self.video = cv2.VideoCapture(self.plik)
        self.video.set(1, self.iterator)
        self.timer.start(1)
        self.seconds_timer.start(1000)
        if self.plik == "/dev/stdin":
            while self.video.grab():
                self.display()
        else:
            self.display()

    def click_pause(self):
        """
        This method contains operations which are connected with "Pause" button. This button stops the GUI timer.
        """
        self.timer.stop()
        self.seconds_timer.stop()
        plt.plot(self.x_coordinates, self.y_coordinates)
        plt.show()

    def display(self):
        """
        Main method of the GUI which is connected with the timer. It is responsible for passing following frames to
        the neural network and then for processing the network output. All operations connected with displaying labeled
        frames and updating information labels are processing down here.
        """
        self.video.set(1, self.iterator)
        status, frame = self.video.read()

        if not status:
            QtCore.QCoreApplication.instance().quit()

        # copy which will be rescaled and used with neural net and tracker
        frame2 = frame.copy()

        if self.first:
            self.first = False
            self.compute_newmatrix(frame)

            init_tracker_frame = frame.copy()
            init_tracker_frame = cv2.resize(init_tracker_frame, (640, 521))

            _ = self.tracker.init(init_tracker_frame, self.tracker_bbox)

        # ----------------------------------------------------------------------------

        # Visual Odometry

        frame = cv2.undistort(frame, self.mtx, self.dist, None, self.newcameramtx)

        # crop the image
        x, y, w, h = self.roi
        frame = frame[y:y + h, x:x + w]
        self.stepper += 1
        if self.stepper % 2 == 0:
            self.prev_kp, self.prev_des, self.prev_img, new_coords, self.R, self.t = odometry(frame, self.prev_kp,
                                                                                              self.prev_des,
                                                                                              self.prev_img,
                                                                                              self.coords,
                                                                                              self.newcameramtx, self.R,
                                                                                              self.t)
            if new_coords is not None:
                self.x_coordinates.append(new_coords[0])
                self.y_coordinates.append(new_coords[2])
                self.straight = math.sqrt(
                    (self.coords[0] - self.x_coordinates[-1]) ** 2 + (self.coords[2] - self.y_coordinates[-1]) ** 2)
                if self.period % 5 == 0:
                    summ = stand(self.x_coordinates[-5:-1], self.y_coordinates[-5:-1])
                    print(summ)
                    if summ < 1:
                        self.odometry_distance = math.sqrt(
                            (self.x_coordinates[-1] - self.x_coordinates[-5]) ** 2 + (
                                    self.y_coordinates[-1] - self.y_coordinates[-5]) ** 2)
                        self.odometry_distance *= self.multiplier
                        self.total_dist += self.odometry_distance
                        self.distance_label.setText(f"Ostatnia balisa: {int(self.total_dist)}cm")
                        self.distance += self.odometry_distance

                    self.period = 1
                self.period += 1
            self.stepper = 1

        # ----------------------------------------------------------------------------

        # Reading information from the neural network and counting objects visible on current frame
        # as well as getting a distance suggestion from feature map

        frame2 = cv2.resize(frame2, (640, 480))

        img, label, self.station, self.points, self.empty_frames, self.feature_distance = detect(frame2, self.points, self.empty_frames, self.distance)

        if self.feature_distance != 0:
            self.feature_label.setText(f"Feature map: {int(self.feature_distance)}cm")
        # ----------------------------------------------------------------------------

        # Using rail-tracker to guess turn direction

        try:
            tracker_status, self.tracker_bbox = self.tracker.update(frame2)
        except NameError:
            print("Tracker update failed")

        if tracker_status:
            # Tracking success - redraw tracker bbox
            p1 = (int(self.tracker_bbox[0]), int(self.tracker_bbox[1]))
            p2 = (int(self.tracker_bbox[0] + self.tracker_bbox[2]), int(self.tracker_bbox[1] + self.tracker_bbox[3]))
            cv2.rectangle(img, p1, p2, (0, 0, 255), 2, 1)  # BGR

        try:
            # Print tracker message if there is any
            traker_message = tracking(self.tracker_bbox)
            if traker_message != "":
                self.tracker_label.setText("Tracker: " + traker_message)
        except NameError:
            print("Tracking failed")

        # ----------------------------------------------------------------------------

        # UI update

        self.trees.setText('Drzewa: ' + str(label.count("tree")))
        QApplication.processEvents()

        self.houses.setText('Budynki: ' + str(label.count("house")))
        QApplication.processEvents()

        self.stations.setText('Stacje: ' + str(label.count("station")))
        QApplication.processEvents()

        self.trains.setText('Pociągi: ' + str(label.count("train")))
        QApplication.processEvents()

        self.bridges.setText('Mosty: ' + str(label.count("bridge")))
        QApplication.processEvents()

        self.viaducts.setText('Wiadukty: ' + str(label.count("viaduct")))
        QApplication.processEvents()

        # Localizing train based on neural network output and BFmatch method

        if label.count("station") + label.count("house") + label.count("tree") == 0:
            self.lokacja.setText('Lokacja:Pustki.')
        elif label.count("station") > 0:
            self.lokacja.setText('Lokacja:Stacja i okolice.')
        elif label.count("house") > 2:
            self.lokacja.setText('Lokacja:Obszar zabudowany.')
        elif label.count("house") == 1:
            if label.count("tree") > 1:
                self.lokacja.setText('Lokacja:Obszar zabudowany.')
        elif label.count("tree") > 3:
            self.lokacja.setText('Lokacja:Obszar zalesiony.')

        QApplication.processEvents()

        # Prepare and display the frame

        height, width, channel = img.shape
        bytesPerLine = 3 * width
        qImg = QImage(img.data, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped()
        pixmap = QPixmap(qImg)

        self.scene = QGraphicsScene()
        self.scene.addPixmap(pixmap)

        self.graphicsView.setScene(self.scene)
        self.graphicsView.adjustSize()
        self.resize(pixmap.size())
        self.iterator += 1


def main():
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = Form()
    mainWindow.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
