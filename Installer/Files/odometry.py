import cv2
import numpy as np
import matplotlib.pyplot as plt

licznik = 1


def detect_features(img):
    """
    This function detects features on provided image using ORB detector algorithm.
    :param img:
    Input image where the features are detected.
    :return:
    kp - list of keypoints detected on input image
    des - list of descriptors (one descriptor per each keypoint)
    """
    fit_quant = 2000
    orb = cv2.ORB_create(fit_quant, edgeThreshold=5)
    kp = orb.detect(img, None)
    kp, des = orb.compute(img, kp)
    return kp, des


def odometry(img, prev_kp, prev_des, prev_img, coords, mtx, R_prev, t_prev):
    """
    This function performs all computations connected with odometry algorithm.
    :param img:
    Input frame.
    :param prev_kp:
    List of keypoints detected on previous frame.
    :param prev_des:
    List of descriptors of keypoints detected on previous frame.
    :param prev_img:
    Previously processed frame.
    :param coords:
    Start point coordinates.
    :param mtx:
    Camera matrix computed during calibration.
    :param R_prev:
    Rotation matrix computed in previous algorithm step.
    :param t_prev:
    Translation matrix computed in previous algorithm step.
    :return:
    prev_kp - keypoint detected on current frame
    prev_des - keypoints descriptors detected on current frame
    prev_img - current frame
    new_coords - coordinates of current train position
    R - currently computed rotation matrix
    t - currently computed translation matrix
    """
    global licznik

    new_coords = None
    R = R_prev
    t = t_prev

    kp, des = detect_features(img)
    if prev_kp is not None and prev_des is not None:
        FLANN_INDEX_LSH = 6
        index_params = dict(algorithm=FLANN_INDEX_LSH, table_number=6, key_size=12, multi_probe_level=1)
        search_params = dict(checks=100)
        flann = cv2.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(np.asarray(prev_des), np.asarray(des), k=2)

        matchesMask = [[0, 0] for _ in range(len(matches))]
        good = []
        for i, match_list in enumerate(matches):
            if len(match_list) != 2:
                continue
            m, n = match_list[0], match_list[1]

            # choose proper matches
            if m.distance < 0.8 * n.distance:
                matchesMask[i] = [1, 0]
                good.append(m)

        kp1 = []
        kp2 = []
        for match in good:
            kp1.append(prev_kp[match.queryIdx].pt)
            kp2.append(kp[match.trainIdx].pt)

        draw_params = dict(matchColor=(0, 255, 0),
                           singlePointColor=(255, 0, 0),
                           matchesMask=matchesMask,
                           flags=0)

        # print matches once per 1000 frames
        licznik += 1
        if licznik % 1000 == 0:
            img3 = cv2.drawMatchesKnn(img, kp, prev_img, prev_kp, matches, None, **draw_params)
            plt.imshow(img3,), plt.show()
            licznik = 1

        # compute R, t matrices
        if len(kp1) > 5:
            E, inliners = cv2.findEssentialMat(np.float64(kp1), np.float64(kp2), mtx, prob=0.999, method=cv2.RANSAC)
            _, R, t, inliners = cv2.recoverPose(E, np.float64(kp1), np.float64(kp2), mtx)

            R = R @ R_prev
            t = t_prev + R_prev @ t
            new_coords = R @ coords + t

    prev_kp = kp
    prev_des = des
    prev_img = img

    return prev_kp, prev_des, prev_img, new_coords, R, t
