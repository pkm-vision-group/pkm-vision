import socket
import threading
from balisa import *


class Server(object):

    def __init__(self, parent):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_address = ('192.168.137.1', 50001)
        self.sock.bind(self.server_address)
        self.sock.listen(1)

        self.client_address = None
        self.connection = None
        self.parent = parent

        self.start_communication()


    def receive_data(self):
        """
          Function that receives data through an endless loop.
          """
        lever = False
        lever2 = 0
        self.balisa_interpreter = Balis_interpreter(self.parent)
        while True:

            try:
                data = self.connection.recv(128)  # odebrane dane
                if data:
                    data = data.decode('utf-8')
                    if len(data) == 21:
                        if lever2 ==1:
                            lever = True
                        lever2 +=1

                    if lever:
                        self.balisa_interpreter.data_input(data[1:9])
            except socket.error:
                self.connection.close()
                break

    def start_communication(self):
        """
           Function that uses threads to simultaneously send and receive data.
           """
        print('Awaiting connection')
        self.connection, self.client_address = self.sock.accept()
        print('Connected with', self.client_address[0])
        receive_thread = threading.Thread(target=self.receive_data)
        receive_thread.start()
        receive_thread.join()
