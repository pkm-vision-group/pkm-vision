# **PKM Vision** :train: ![build status](https://i.imgur.com/t74Dm81.png)
# Znajdowanie pozycji lokomotywy na podstawie obrazu z kamery
#### Autorzy:
* ##### Mikołaj Badocha
* ##### Sebastian Łużyński
* ##### Monika Rogulska
* ##### Marcin Świniarski
* ##### Filip Adamus
* ##### Jan Mikicki
* ##### Krzysztof Sulewski
* ##### Patryk Wittbrodt
* ##### Piotr Winkler
***
### Cel Projektu
Celem głównym było stworzenie zintegrowanego systemu nawigacji lokomotywy, z wykorzystaniem elementów wizyjnych, takich jak kamera, czujników oraz `Raspberry Pi 0`. Docelowo, system miał wyznaczać odległość od początku trasy,odległości do poszczególnych stacji i punktów charakterystycznych. Implementacja oprogramowania przeprowadzona została z użyciem języka `Python`
##### Główne założenia i problemy:
* Zainstalowanie `Raspberry` wraz z kamerą na lokomotywie
* Znalezienie odpowiedniego źródła zasilania
* Implementacja skryptów do streamowania obrazu i automatycznego uruchamiania przy restarcie `Raspberry`
* Implementacja skryptów do odbierania i obsługi streamu
* Interfejs graficzny
* Instalator
* Próba wykorzystania akcelerometru, `optical flow`, czujników odległości, optycznego pomiaru odległości
*  Implementacja sieci neuronowej wykrywającej stacje, drzewa, domki, inne pociągi oraz mosty 
* Odczytywanie informacji z czujników Halla, umieszczonych w torach
***
### Wykorzystane narzędzia
* `Raspberry Pi Zero`
* Kamera do `Raspberry`
* Biblioteka do GUI`PyQt 5`
* `opencv`
* Skrypt instalatora przy użyciu `NSIS`
* `Python 3`
* Czujniki Halla
### Uruchomienie
* Podłączenie raspberry pi do zasilania
* Właczenie programu na komputerze laboratoryjnym znajdującym się na pulpicie w folderze PKM_VISION_2019
* Dopilnowanie czy raspberry pi, komputer laboratoryjny i komputer na którym odpalany jest program znajdują się w tej samej sieci
* Uruchomienie programu

***
`track.py`
Jest to skrypt śledzący tory. W tym celu używamy biblioteki `OpenCV 3`, która zawiera
implementacje algorytmów śledzących pojedynczy obiekt. Spośród 8 dostępnych tracker’ów
używamy algorytmu CSRT (Discriminative Correlation Filter with Channel and Spatial Reliability).
Operuje na mniejszej ilości klatek na sekundę w porównaniu z innymi wbudowanymi algorytmami,
lecz posiada zdecydowanie lepszą dokładność śledzenia obiektów.

Region of Interest jest predefiniowany, aby nie trzeba było ustalać go przy każdym załączeniu
stream’u i oznaczony jest czerwonym bounding box’em.

Wyciągane jest średnie położenie z zadanej ilości klatek i porównywane jest ono z punktem
odniesienia, tj. z pozycją bounding box’a, kiedy pociąg jedzie po prostej. Określona zmiana
średniego położenia jest uznawana za zakręt - jej wartość jest dynamicznie zmieniana na
poszczególnych trasach, aby eliminować występowanie nieprawidłowych odczytów. Początek i
koniec zakrętu są punktami charakterystycznymi dodawanymi do puli informacji o przebiegu
trasy.

`rpi.py`
Jednym z fundamentów projektu PKM Vision było stworzenie niezawodnego streamu z kamery raspberry Pi. Głównym założeniem było możliwie jak najmniejsze opóźnienie, a w późniejszym etapie projektu możliwość bezproblemowego dostępu do streamu przez wszystkich członków grupy. Z powodu dużych problemów z uruchomieniem streamu przez netcat na innych urządzeniach, ostatecznym rozwiązaniem jest stream z kamery na server HTTP, do którego jest łatwy dostęp z każdego urządzenia będącego w sieci Wi-Fi z raspberry. Utworzony skrypt na raspberry, służący do stworzenia, uruchomienia i przesłania przez socket na server HTTP obrazu live z kamery. Jest oparty na bibliotekach threading, io, logging, socketserver, http. Użyte klasy, funkcje i metody są opisane poniżej. 

`auto_stream.py`
W finalnej wersji PKM Vision uruchomienie streamu i połączenie z GUI następuje z poziomu GUI. Wszystko jest uruchamiane jednym skryptem, zamieszczonym w GUI. Do połączenia z raspberry, skrypt wykorzystuje połączenie SSH z użyciem biblioteki paramiko.

`odometry.py`
Jest to skrypt odpowiedzialny za wykonywanie obliczeń związanych z wyznaczaniem trajektorii pociągu na bazie kolejnych klatek obrazu z kamery. 
Najpierw za pomocą ORB detektora wyznaczane są punkty charakterystyczne danego obrazu. Następnie punkty te są parowane z punktami wykrytymi 
na poprzedniej klatce za pomocą algorytmu Flann. Zdefiniowane w ten sposób pary są przefiltrowywane w celu zachowania jak największej ilości 
poprawnych połączeń. 
Na ich podstawie wyznaczana jest macierz relacji za pomocą funkcji opencv "findEssentialMat". Z niej z kolei za pomocą funkcji "recoverPose" 
wyodrębniane są macierze translacji oraz rotacji. Określają one odpowiednio przesunięcie oraz obrót jakim podlegał pociąg pomiędzy poszczególnymi
klatkami obrazu. Modyfikacja dotychczasowej trajektorii za pomocą wyliczonych macierzy pozwala określić w każdym kroku punkt końcowy, w jakim 
znalazł się pociąg względem punktu startowego.

`calibration.py`
Jest to skrypt użyty w procesie kalibracji kamery. W celu poprawnego działania odometrii konieczne jest wyznaczenie ogniskowej kamery podanej w pikselach
oraz współrzędnych tzw. punktu centralnego obrazu widzianego przez wspomnianą kamerę. Istotną kwestią jest również niwelowanie zniekształceń promieniowych i
stycznych powodujących pojawianie się na obrazie m.in. efektów "beczki", czy "rybiego oka". Możliwe jest to poprzez wprowadzenie odpowiednich współczynników 
wyznaczanych, podobnie jak wspomniana ogniskowa, czy punkt centralny, w procesie kalibracji. 
Proces ten polega na wykonaniu kamerą serii zdjęć wzorca ułożonego w różnych konfiguracjach. W przypadku tego projektu zastosowanym wzorcem była czarno-biała
szachownica. Następnie zdjęcia te poddawane są obróbce w przygotowanym skrypcie, który za każdym razem odczytyje punkty charakterystyczne wzoru. Gdy proces ten
dobiegnie końca wydobyte dane w porównaniu z wprowadzonymi przez użytkowanika rzeczywistymi wymiarami wzorca pozwalają wyliczyć macierz kamery oraz współczynniki 
odpowiedzialne za niwelowanie zniekształceń. Odpowiada za to funkcja opencv "calibrateCamera".
Na zakończenie wyliczone dane zapisywane są do pliku xml w celu późniejszego wykorzystania we wspomnianej odometrii.

`GUI_detection.py`
Ten skrypt pośredniczy w komunikacji pomiędzy GUI, a skryptem "detection_functions.py".

`detection_functions.py`
Ten skrypt odpowiada za operacje związane z działaniem sieci neuronowej wykrywającej obiekty wokół pociągu. Zastosowana została architektura yolo-tinyv3, czyli
obecne (2019) state-of-the-art w detekcji obiektów za pomocą sztucznych sieci neuronowych. 
Architektura sieci, jak i wytrenowane uprzednio wagi wczytywane są do programu z wykorzystaniem biblioteki open-cv, a sama sieć wykorzystywana jest już wyłącznie 
w zakresie inferencji. 
Przytoczony skrypt przepuszcza otrzymywane klatki obrazu przez sieć, a także przetwarza jej odpowiedzi w celu wytworzenia bounding-boxów i przypisania im odpowiednich
labeli. 
W tym miejscu zaimplementowana została również funkcjonalność rozpoznawania stacji za pomocą metody "BF match" porównującej otrzymywane klatki nagrania z odpowiednio 
oznaczonymi zdjęciami zapisanymi w bazie programu.

`augmentation.py`
Prosty skrypt, którego zadaniem jest zwiększenie ilości danych do szkolenia sieci neuronowej. Przyjmuje on na wejście obraz i na jego podstawie wytwarza nowe zdjęcia
wykorzystując takie operacje, jak rotacja, czy zmiana perspektywy.

`fature_map.py`
Skrypt odpowiadający za działanie algorytmu Feature Map, którego zadaniem jest przeszukanie skonstruowanej bazy danych i
znajdowanie punktow najbardziej prawdopodobnie odpowiadającym aktualnej klatce obrazu. Jeżeli dany punkt z pewnym prawdopodobieństwem odpowiada
punktowi z bazy danych - aktualna pozycja pociągu od stacji początkowej jest korygowana. 

`balisa.py`
Skrypt odpowiadający za przetwarzanie danych z balis otrzymanych przez serwer a następnie skojarzenie ich z odpowiednią stacją i przesłanie odpowiedniego sygnału z wartością odległosci do GUI. Oblicza również chwilową prędkość pociągu. 

`server.py`
Skrypt odpowiadający odbieranie informacji o przejeździe nad balisą
***

``` python
calibration.py

calibrate(images_path, xml_path):
    """
    This function calibrates camera based on provided 
    dataset containing proper chessboard images.
    
    :param images_path:
    Source file containing calibration images.
    :param xml_path:
    Path to destination xml file.
    """
```

``` python
odometry.py

detect_features(img):
    """
    This function detects features on provided image using ORB detector algorithm.
    
    :param img:
    Input image where the features are detected.
    
    :return:
    kp - list of keypoints detected on input image
    des - list of descriptors (one descriptor per each keypoint)
    """
    
odometry(img, prev_kp, prev_des, prev_img, coords, mtx, R_prev, t_prev):
    """
    This function performs all computations connected with odometry algorithm.
    
    :param img:
    Input frame.
    :param prev_kp:
    List of keypoints detected on previous frame.
    :param prev_des:
    List of descriptors of keypoints detected on previous frame.
    :param prev_img:
    Previously processed frame.
    :param coords:
    Start point coordinates.
    :param mtx:
    Camera matrix computed during calibration.
    :param R_prev:
    Rotation matrix computed in previous algorithm step.
    :param t_prev:
    Translation matrix computed in previous algorithm step.
    
    :return:
    prev_kp - keypoint detected on current frame
    prev_des - keypoints descriptors detected on current frame
    prev_img - current frame
    new_coords - coordinates of current train position
    R - currently computed rotation matrix
    t - currently computed translation matrix
    """
```

``` python
OptiDist.py

find_marker(image):
    """ Searches for marker using pattern

    Args:
        image: cv2 image where to find marker

    Returns bounds of marker
    """
    
distance(knownWidth, focalLength, perWidth):
    """ Returns distance from camera to object

    Args:
        knownWidth: width of marker
        focalLength: parameter of camera foci
        perWidth: width seen by camera

    Returns:
        distance from marker to camera
```

``` python
GUI_detection.py

detect(frame, points, empty_frames):
    """
    This function passes frames of record received from GUI forward to detection_functions for processing
    in neural network. It also manages the work of "BF match" algorithm which is used to detect which station was
    reached by the train after the neural network indicated that train reached some station.
    
    :param frame:
        Current frame of record.
    :param points:

    :param empty_frames:
    
    :return:
        Output image with bounding boxes on it. List of labels for detected objects. Station number which
        was recognized by "BF match" algorithm.
    """

parse_args():
    """
    This function allows to change program parameters while running from command line.
    :return:
        Object containing values of all listed arguments.
    :Example:
        To run program with e.g. confidence threshold value equal to 0.5 and scale value equal to 0.1
        go to project directory and type in command line:
            python detection.py -c 0.5 -s 0.1
    """
```

``` python
detection_functions.py

populate_class_labels(names):
    """
    This function extracts names of classes from input .txt file (each class has to be in separate line).
    
    :param names:
        Path to input .txt file.
        
    :return:
        List of classes names.
    """

get_output_layers(network):
    """
    This function returns names of neural network layers which have unconnected outputs.
    :param network:
        Used neural network.
    :return:
        List of layers names.
    """

draw_bbox(img, bbox, labels, confidence, names, colors=None, write_conf=False):
    """
    This function uses neural network outputs to draw bounding boxes on processed images.
    
    :param img:
        Currently processed image.
    :param bbox:
        List of lists. Each inside list contains four coordinates needed to draw each bounding box.
    :param labels:
        List of labels ordered accordingly to the order of bounding boxes.
    :param confidence:
        List of confidence values for each detection.
    :param names:
        Path to input .txt file containing names of classes.
    :param colors:
        List of bounding boxes layers. None by default.
    :param write_conf:
        Change value to True for writing confidence values next to classes names.
        
    :return:
        Image with proper bounding boxes.
    """

detect_common_objects(image, model, weights, names, conf, nms, sc):
    """
    This function processes outputs from neural network and orders them in proper containers.
    :param image:
        Currently processed image.
    :param model:
        Path to neural network model saved in .cfg file.
    :param weights:
        Path to neural network weights saved in .weights file.
    :param names:
        Path to input .txt file containing names of classes.
    :param conf:
        Value of confidence threshold.
    :param nms:
        Value of non-maximum suppression threshold.
    :param sc:
        Value of input image scale using in pre-processing.
    :return:
        Lists of bounding boxes, labels and confidence values.
    """

bfmatch(frame, cnt):
    """
    This function recognizes which station is reached by train.
    
    :param frame:
        Current frame of record.
    :param cnt:
        Counter value. When it's equal to 10 information about reached station is passed backward to GUI_detection.
        
    :return:
        Number which indicates which station was reached by train.
    """
```

``` python
server.py


receive_data(self):
    """
    Function that receives data through an endless loop.
    """
    
start_communication(self):
    """
    Function that uses threads to simultaneously send and receive data. 
    """
```

``` python
balisa.py

data_input(self, data):
    """
        Function that performs operations basing on signals from Hall sensors.

        :param data:
        ID number of a recognised Hall sensor
    """


reset(self):
    """
        Function that resets odometry in parent program.
    """
    
     	
velocity(self, data):
    """
        Function that calculates velocity based on a difference in time
        between two signals from a single Hall sensor.

        :param data:
            ID number of a recognised Hall sensor
    """
```
``` python
rpi.py

class StreamingOutput(object):
    write(self, buf):
    """
    This class is responsible for writing frame to the buffer.
    It creates new frame, copy the existing buffer's content and notify all
    clients it's available.

    :param buf:
    Existing buffer

    :return:
    writes the value of the existing buffer

    Methods:
    * Condition() - A condition variable allows one or more threads to wait until they are notified by another thread.
    * notify_all() - Wake up all threads waiting on this condition.
    * BytesIO() - Expects bytes-like objects and produces bytes objects.
    * truncate() - Resize the stream to the given size in bytes.
    * getvalue() - Return bytes containing the entire contents of the buffer.
    * write(arg) - Write the given bytes-like object, arg,  to the underlying raw stream, and return the number of bytes written.
    * seek(offset[,whence]) - Change the stream position to the given byte offset.
    """

class StreamingHandler(server.BaseHTTPRequestHandler):
    do_GET(self):
    """
    Class that handles the server. It's creating the proper structure
    of the http server with headers, handling frames to the server and clients.

    :param server.BaseHTTPRequestHandler:
    This param is used to handle the HTTP requests that arrive at the server.
    It provides a number of class and instance variables, and methods for use by subclasses.

    Methods:
    * send_response(code[,message]) - Sends a response header and logs the accepted request.
    * send_header(keyword, value) - Writes a specific HTTP header to the output stream.
    * end_headers() - Sends a blank line, indicating the end of the HTTP headers in the response.
    * wait(timeout=None) - Wait until notified or until a timeout occurs.
    * send_error(code[,message]) - Sends and logs a complete error reply to the client.
    """

class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):

    """
    This class is responsible for creating the stream to the HTTP server with picamera record
    used as an output. It starts the record of picamera in mjpeg format and creates infinite server
    with specified port and raspberry PI IP address.

    :param socketserver.ThreadingMixIn:
    Creates thread to handle each request; ThreadingMixIn mix-in is used to support asynchronous behaviour.

    :param server.HTTPServer:
    This param builds on the TCPServer class by storing the server address as instance variables named server_name and server_port.
    The server is accessible by the handler in StreamingHandler class

    Methods:
    * start_recording(output, format=None, resize=None, splitter_port=1, **options) - Start recording video from the camera, storing it in output.
    * serve_forever(poll_interval=0.5) - Handle requests until an explicit shutdown() request.
    * stop_recording() - stop the record of the picamera.
    """

```
``` python
auto_stream.py 

launch_stream(ip_addr):
    """
    This function is launching the stream by connecting with raspberry pi via SSH and
    executes the python script on raspberry to create stream.

    :param ip_addr:
    IP address to the raspberry pi, which is changing constantly with Wi-Fi network changes.

    Methods:
    * SSHClient() - A high-level representation of a session with an SSH server.
    This class wraps Transport, Channel, and SFTPClient to take care of most aspects of authenticating and opening channels.
    * AutoAddPolicy() - Policy for automatically adding the hostname and new host key to the local HostKeys object, and saving it.
    * set_missing_host_key_policy(policy) - Set policy to use when connecting to servers without a known host key.
    * connect(hostname, port=22, username=None, password=None, pkey=None, key_filename=None, timeout=None, allow_agent=True,
    look_for_keys=True, compress=False, sock=None, gss_auth=False, gss_kex=False, gss_deleg_creds=True, gss_host=None,
    banner_timeout=None, auth_timeout=None, gss_trust_dns=True, passphrase=None) - Connect to an SSH server and authenticate to it.
    Used parameters:
    - hostname (str) – the server to connect to
    - username (str) – the username to authenticate as
    - password (str) – Used for password authentication
    * exec_command(command, bufsize=-1, timeout=None, get_pty=False, environment=None) - execute a command on the SSH server.
    The command’s input and output streams are returned as Python file-like objects representing stdin, stdout, and stderr.
    Used parameters:
    - command (str) – the command to execute
    Returns:
    the stdin, stdout, and stderr of the executing command, as a 3-tuple
    Raises:
    SSHException – if the server fails to execute the command
    """
```
``` python
feature_map.py 

read_data(path):
    """
    Reads data from file located in given path and converts it into list of bounding boxes, labels and distance.
    Collected data are preprocessed to use in feature maps.
    
    :param path: path to database of bounding boxes, labels and distance
    :return: preprocessed list of bounding boxes[0], labels[1], distance[2]
    """
    
compare_data(database, bboxes, label, startpoint):
    """
    This function takes preprocessed database, in time detected bounding boxes, labels and compares it with database.
    In case of numbers of good comparsions and the confidence higher than given, the distance of the train
    from starting point is returned.
    
    :param database: preprocessed database of bounding boxes, labels and distance
    :param bboxes: in time bounding boxes of detected objects
    :param label: in time labels of detected objects
    :param startpoint: the point where we start(in reference of Kiełpinek station)
    :return: last point where the detected objects where detected, the distance of the train from starting point
    """
    
encode_labels(labels):
    """
    Encodes labels into valid ones
    :param labels: not valid labels
    :return: valid labels
    """
```
***

### Instalator
Do wykonania instalatora użyto technologii `NSIS`. Jest to skrypt, w którym definiuje się strukturę okienek instalatora, podaje się ścieżki wypakowania plików i tworzy wymagane, dodatkowe pliki. Pozwala również na wykonwyanie innych skryptów np. batchowych w celu utworzenia wirtualnego środowiska `Python` i zainstalowaniu potrzebnych bibliotek. Spis modułów znajduje się w pliku `requierments.txt`.
